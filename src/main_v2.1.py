import logging
import sys
import os
import random
from datetime import date
import piexif
import math
import telegram_send
import telegram
import requests
# Library for structured extraction
from collections import namedtuple
# Library for data cleaning
import re
# Libraries for images
from PIL import Image
from PIL.ExifTags import TAGS

# Create and configure logger
pathLogs = "logs/"
pathSend = "../send/"
pathReceive = "../receive/"
if not os.path.exists(pathLogs):
    os.makedirs(pathLogs)
if not os.path.exists(pathSend):
    os.makedirs(pathSend)
if not os.path.exists(pathReceive):
    os.makedirs(pathReceive)

fileLogs = "logs/" + str(date.today()) + ".log"

logging.basicConfig(filename=fileLogs,
                    format='%(asctime)s [%(levelname)s] %(message)s',
                    filemode='a',
                    level=logging.INFO)


# Function to read metadata that returns the important information structured
def readMetadata(fpath):
    Meta = namedtuple('Meta', ['filename', 'order', 'blocks'])
    logging.info("Retrieving files from path \"%s\"" % (fpath))
    # Retrieving only the .webp files
    files = []
    for root, d, fil in os.walk(fpath):
        for file in fil:
            if '.webp' in file:
                files.append(os.path.join(root, file))

    logging.info("Reading metadata of files to get ours")
    # Looking for stego stickers to discard the noise stickers
    ourstickers = []
    for f in files:
        im = Image.open(f)
        try:
            for k, v in im._getexif().items():
                if TAGS.get(k, k) == "Software" and v == "QKQ1.191002.002 V11.0.18.0.QDLMCEV":
                    ourstickers.append(f)
        except Exception as e:
            logging.warning(" \"%s\" dont have metadata" % (f))
    logging.info(ourstickers)
    # Extract the information from the metadata of the stego stickers
    structuredinfo = []
    for s in ourstickers:
        im = Image.open(s)
        for k, v in im._getexif().items():
            if TAGS.get(k, k) == "Sharpness":
                orden = v
            if TAGS.get(k, k) == "LensMake":
                bloq = v
        m = Meta(s, orden, bloq)
        structuredinfo.append(m)
    logging.info(structuredinfo)
    return structuredinfo

# Obtain original length of the message
def obtain_higher_number(structuredinfo):
	max = 0
	for li in structuredinfo:
		if li.order > max:
			max = li.order
	return max + 1

# Function to read the hidden msg using the params extracted from the metadata
def read_hidden_msg(structuredinfo):
    msglen = obtain_higher_number(structuredinfo)
    portions = [""] * msglen
    for li in structuredinfo:
        part = read_content(li)
        portions[msglen - int(li.order) - 1] = part
    message = ''.join(portions)
    return message


# Function to read the portion of the message hidden in each file
def read_content(meta):
    filename = meta.filename
    block = meta.blocks
    long = int(block[-3:-1]) * 4
    mode = block[-1:]
    # Extract from the readed file the number of blocks and the hidden characters
    f = open(filename, 'rb')
    f.seek(-long, os.SEEK_END)
    contentbinary = f.read()
    f.close()
    # Convert to ASCII and clean the output to extract letters according to code
    content = contentbinary.decode()
    regex = re.compile('[^a-zA-Z ,.]')
    cleancontent = regex.sub('', content)
    regex = re.compile('^[ ]')
    cleancontent = regex.sub('', cleancontent)
    fullletters = list(cleancontent)
    if mode == "i":
        start = 0
    else:
        start = 1
    extractedletters = fullletters[start::2]

    logging.debug(long, mode, fullletters, extractedletters)
    portion = ''.join(extractedletters)
    return portion


# Function to modify metadata
def writeMetadata(fOriginal, fModified, hiddenMessage, stickersLeft):
    logging.info("Writing metadata of original file \"%s\" to \"%s\"" % (fOriginal, fModified))
    zeroth_ifd = {piexif.ImageIFD.Make: u"Xiaomi",
                  piexif.ImageIFD.XResolution: (96, 1),
                  piexif.ImageIFD.YResolution: (96, 1),
                  piexif.ImageIFD.Software: u"QKQ1.191002.002 V11.0.18.0.QDLMCEV"
                  }
    exif_ifd = {piexif.ExifIFD.DateTimeOriginal: u"2017:11:11 05:28:10",
                piexif.ExifIFD.LensMake: hiddenMessage,
                piexif.ExifIFD.Sharpness: stickersLeft,
                piexif.ExifIFD.LensSpecification: ((1, 1), (1, 1), (1, 1), (1, 1)),
                }
    gps_ifd = {piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
               piexif.GPSIFD.GPSAltitudeRef: 1,
               piexif.GPSIFD.GPSDateStamp: u"2017:11:11 06:28:10",
               }
    first_ifd = {piexif.ImageIFD.Make: u"Xiaomi",
                 piexif.ImageIFD.XResolution: (40, 1),
                 piexif.ImageIFD.YResolution: (40, 1),
                 piexif.ImageIFD.Software: u"QKQ1.191002.002 V11.0.18.0.QDLMCEV"
                 }

    exif_dict = {"0th": zeroth_ifd, "Exif": exif_ifd, "GPS": gps_ifd, "1st": first_ifd}

    im_test = Image.open(fOriginal)

    exif_bytes = piexif.dump(exif_dict)
    im_test.save(fModified, "webp", exif=exif_bytes)


# Function to obtain different stickers from a pull of stickers
def obtain_stickers(number, pathOfResources):
    stickers = []
    i = 0
    while i < number:
        stickers.append(os.path.splitext(random.choice(os.listdir(pathOfResources)))[0])
        i = i + 1
    return stickers


# Return random string n characters
def obtain_random_message(n):
    value = ''.join(random.choice('ab0cd1ef2gh3ij4kl5mn6op7qr8st9uvwxyz') for _ in range(n))
    return value


# Return random letter
def obtain_random_letter():
    value = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(1))
    return value


# Return random "p" (par) or "i" (impar)
def obtain_block_to_read():
    value = ''.join(random.choice('ip') for _ in range(1))
    return value


# Function to write blocks of data in stickers
def writeContent(filename, actualMessage, oddOrEven):
    mixedMessage = ""
    for letter in actualMessage:
        if oddOrEven == 'p':
            mixedMessage = mixedMessage + obtain_random_letter() + letter
        elif oddOrEven == 'i':
            mixedMessage = mixedMessage + letter + obtain_random_letter()

    hexaActual = bytes(mixedMessage, 'utf-8')
    logging.debug(hexaActual)
    f = open(filename, 'rb')
    content = f.read()
    f.close()
    content = content + hexaActual
    f = open(filename, 'wb')
    f.write(content)
    f.close()


# main
def main():
    logging.info("*** Starting stego tool ***")

    # 1. Initial settings
    if ( (len(sys.argv) < 2) or (sys.argv[1] == 0 and len(sys.argv) < 4) or (sys.argv[1] == 1 and len(sys.argv) < 2) ):
        print("Use of script: ./main.py 0 (0: encrypt) n (1-99: sizeOfChunk) pathStickers pathToSave")
        print("Use of script: ./main.py 1 (1: decrypt) pathToRead")
        exit()
    try:
        option = int(sys.argv[1])
        if option == 0:
            sizeOfChunk = int(sys.argv[2])
            path = sys.argv[3]
            pathSend = sys.argv[4]
            if not os.path.isdir(path) or not os.path.isdir(pathSend):
                print("Incorrect path to directories")
                print("Use of script: ./main.py 0 (0: encrypt) n (1-99: sizeOfChunk) pathStickers pathToSave")
                exit()
        elif option == 1:
            path = sys.argv[2]
            if not os.path.isdir(path):
                print("Incorrect path to directory")
                print("Use of script: ./main.py 1 (1: decrypt) pathToRead")
                exit()
    except Exception:
        print("Use of script: ./main.py 0 (0: encrypt) n (1-99: sizeOfChunk) pathStickers pathToSave")
        print("Use of script: ./main.py 1 (1: decrypt) pathToRead")
        exit()
    extension = ".webp"
    bot = telegram.Bot(token= "1799479688:AAEHyut1OezqqKefCsrsZePwi1yQbXdhuEw")

    # 2. Modify metadata
    if option == 0:  # Insert data in stickers
        logging.info("Insert metadata in stickers option")
        # 2.1. Reading hidden message to insert data in sticker
        if sizeOfChunk < 1 or sizeOfChunk > 99:
            sizeOfChunk = 20
            print("sizeOfChunk contains a value out of the range 1-99, setting to default value 20")
        message = str(input("1. Insert your message (each %d characters 1 sticker needed): " % sizeOfChunk))
        lengthMessage = len(message)
        neededStickers = int(math.ceil(lengthMessage / sizeOfChunk))
        print("2. The message length is %d, %d stickers are needed" % (lengthMessage, neededStickers))

        # 2.2. Choose stickers
        stickers = obtain_stickers(neededStickers, path)
        print("3. Randomly selected from the sticker's pool: %s" % stickers)

        # 2.3. Create loop for multiple stickers
        logging.info("\n***** STICKERS LOOP *****\n")
        i = 0
        print("4. Sending via Telegram API")
        file_ids = []
        while i < neededStickers:
            actualMessage = message[sizeOfChunk * i:sizeOfChunk * i + sizeOfChunk]
            logging.info("Message has: %s characters" % len(actualMessage))
            logging.info("Sticker %s contains message chunk: %s" % (stickers[i], actualMessage))

            fullName = path + stickers[i] + extension
            fullNameModified = pathSend + str(i + 1) + extension

            # 2.4. Modify metadata
            logging.info("Writing metadata: ")
            lenActualMessage = format(int(len(actualMessage)), '02')

            # Create secret message to know how to read later the hidden info
            oddOrEven = obtain_block_to_read()
            secretMessage = obtain_random_message(13) + str(lenActualMessage) + str(oddOrEven)
            logging.info("Final secret message: %s" % secretMessage)

            # 2.5. Finally, writing the metadata
            writeMetadata(fullName, fullNameModified, secretMessage, (neededStickers - i - 1))

            # 2.6. Modify the real content of sticker
            writeContent(fullNameModified, actualMessage, oddOrEven)
            i = i + 1
            # 2.7. Send modified stickers through Telegram (API)
            with open(fullNameModified, "rb") as f:
                botResponse = bot.sendSticker(chat_id='-1001127912200', sticker=f)
                file_ids.append(botResponse.sticker.file_id)
            logging.info("\n ****************************************************** \n")
        #2.8. Send IDs file
        print("5. Sending IDs to channel")
        outF = open(pathSend + "ids.txt", "w")
        for line in file_ids:
          # write line to output file
          outF.write(line)
          outF.write("\n")
        outF.close()
        bot.sendMessage(chat_id = '-1001127912200', text = 'If you want to download your stickers use these ids:')
        with open("../send/ids.txt", "rb") as f:
            bot.sendDocument(chat_id='-1001127912200', document=f)
        print("6. Message Sent!!!!!!!!")
    # 3. Read metadata
    elif option == 1:  # Descrypt data from stickers
        logging.info("Decrypt metadata from stickers option")
        content_array = []
        with open(path + '/ids.txt') as f:
                #Content_list is the list that contains the read lines.
                for line in f:
                    str2 = line.replace("\n", "")
                    content_array.append(str2)
        number = 1
        for ids in content_array:
            response = bot.getFile(ids)
            r = requests.get(response.file_path)
            print(r)
            with open('../receive/' + str(number) +'.webp', 'wb') as f:
                f.write(r.content)
            number = number + 1
        # 3. 1. Read from folder, Telegram, ...
        print("1. Reading stickers from folder %s" % path)
        metainfo = readMetadata(path)
        # 3. 2. Extract and transform message to human readable
        print("2. Obtaining message")
        message = read_hidden_msg(metainfo)
        print("3. Here is your message: %s" % (message))

    else:
        print("Incorrect option. Valid options: 0 (encrypt) or 1 (decrypt). Exiting...")
        exit()

# Clean folder with sended stickers
    #if os.path.exists(pathSend):
     #   os.rmtree(pathSend)
    #if not os.path.exists(pathSend):
     #   os.makedirs(pathSend)
    logging.info("********************* END! *********************\n")


main()
