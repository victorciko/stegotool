**INSTALACIÓN PREVIA**
```
sudo pip3 install -r requirements.txt
```

**ENVIAR**
1. Descargar proyecto y tener las carpetas de resources con un pool de stickers y la carpeta de send donde se guardarán los creados
2. Ejecutar herramienta:

**Linux:** ``` sudo python3 main.py 0 ../resources/ ../send/ ```

**Windows:** ``` python.exe .\main.py 0 ..\resources\ ..\send\ ```


**RECIBIR**
1. Unirse al canal de telegram al que se envían las stickers: `https://t.me/joinchat/JIGxxXfN3s80NjA8`
2. Descargar Stickers del bot a una carpeta (receive) para que pueda leer la herramienta de ahí
3. Ejecutar herramienta:

**Linux:** `sudo python3 main.py 1 ../receive/`

**Windows:** `python.exe .\main.py 1 ../receive/`
